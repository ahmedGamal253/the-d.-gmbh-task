//
//  ViewController.swift
//  TheD.GmbHTask
//
//  Created by Ahmed Gamal on 8/29/19.
//  Copyright © 2019 Ahmed Gamal. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {

    @IBOutlet weak var tableItems: UITableView!
    @IBOutlet weak var btnRetry: UIButton!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    @IBOutlet weak var labelError: UILabel!
    
    var isOffline = false
    var page = 1
    var itemsArray = [ItemModel]()
    let coreData = AppCoreData.instance
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initTableCell()
        checkInternet()
        // Do any additional setup after loading the view.
    }
    
    func checkInternet(){
        if  AppConnectionsHandler.instance.checkConnection(){
            isOffline = false
            indicator.isHidden = false
            labelError.isHidden = true
            btnRetry.isHidden = true
            tableItems.isHidden = true
            AppCoreData.instance.removeItems()
            getData()
        }else{
            isOffline = true
            AppSnackBarHandler.instance.showMessage(message: "no interner connection")
            if coreData.getSavedItems().count == 0{
                indicator.isHidden = true
                labelError.isHidden = false
                btnRetry.isHidden = false
                tableItems.isHidden = true
            }else{
                tableItems.isHidden = false
                indicator.isHidden = true
                btnRetry.isHidden = true
                labelError.isHidden = true
                stopPullTable()
                tableItems.reloadData()
            }
            //show offline data
        }
    }
    
    func stopPullTable(){
        tableItems.es.stopPullToRefresh(ignoreDate: true)
        tableItems.es.stopPullToRefresh(ignoreDate: true, ignoreFooter: false)
        tableItems.es.stopLoadingMore()
    }
    
    func getData(){
        let url = AppUrl.instance.getItemsURL(page: "\(page)")
        let instance = AppConnectionsHandler()
        instance.responseClosure = {(responseType:Int, response:[String:Any]?) in
            self.indicator.isHidden = true
            self.stopPullTable()
            switch responseType {
            case instance.sucess:
                self.tableItems.isHidden = false
                let model = ItemsResponseModel(response!)
                if self.page == 1{
                    self.itemsArray.removeAll()
                    self.coreData.removeItems()
                }
                if model.items.count != 0{
                    self.itemsArray.append(contentsOf: model.items)
                    self.page += 1
                    self.cachDataInCoreData(items: model.items)
                }else{
                    self.tableItems.es.noticeNoMoreData()
                }
                self.tableItems.reloadData()
                break
            case instance.sucessWithError:
                self.labelError.isHidden = false
                self.btnRetry.isHidden = false
                self.labelError.text = "error in connection"
                break
            case instance.failServer:
                self.labelError.isHidden = false
                self.btnRetry.isHidden = false
                self.labelError.text = "error in connection"
                break
            default:
                AppSnackBarHandler.instance.showMessage(message: "no interner connection")
                break
            }
        }
        instance.get(url: url)
    }
    
    func cachDataInCoreData(items:[ItemModel]){
        for item in items{
            coreData.addItem(model: item)
        }
    }
    
    @IBAction func retry(_ sender: Any) {
        checkInternet()
    }


}

