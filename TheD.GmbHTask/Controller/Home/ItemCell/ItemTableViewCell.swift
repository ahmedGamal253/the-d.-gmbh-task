//
//  ItemTableViewCell.swift
//  CashUTask
//
//  Created by Ahmed Gamal on 8/22/19.
//  Copyright © 2019 Ahmed Gamal. All rights reserved.
//

import UIKit

class ItemTableViewCell: UITableViewCell {

    @IBOutlet weak var viewCell: UIView!
    @IBOutlet weak var labelWatcherNumber: UILabel!
    @IBOutlet weak var labelLanguage: UILabel!
    @IBOutlet weak var labelDescription: UILabel!
    @IBOutlet weak var labelName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setBorder()
        // Initialization code
    }

    func setBorder() {
        viewCell.layer.cornerRadius = 5
        viewCell.layer.borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        viewCell.layer.borderWidth = 1
    }
    
    func configCell(item:ItemModel){
        labelName.text = item.name
        labelDescription.text = item.description
        labelLanguage.text = "Language: \(item.language)"
        labelWatcherNumber.text = "watchers: \(item.watchers_count)"
    }
    
    // override func to show item from core data
    func configCell(item:Item){
        labelName.text = item.name ?? ""
        labelDescription.text = item.descri ?? ""
        labelLanguage.text = item.language ?? ""
        labelWatcherNumber.text = "watchers: \(item.watchers_count ?? "")"
    }
}
