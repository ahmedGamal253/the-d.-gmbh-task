//
//  HomeViewControllerExtension.swift
//  TheD.GmbHTask
//
//  Created by Ahmed Gamal on 8/22/19.
//  Copyright © 2019 Ahmed Gamal. All rights reserved.
//

import Foundation
import UIKit
import ESPullToRefresh

extension HomeViewController : UITableViewDataSource,UITableViewDelegate {
    
    func initTableCell() {
        let nib1 = UINib(nibName: "ItemTableViewCell", bundle: nil)
        tableItems.register(nib1, forCellReuseIdentifier: "ItemTableViewCell")
        tableItems.estimatedRowHeight = 80
        tableItems.rowHeight = UITableView.automaticDimension
        tableItems.es.addPullToRefresh {
            [weak self] in
            self?.page = 1
            if self!.isOffline{
                self?.checkInternet()
            }else{
                self?.getData()
            }
        }
        self.tableItems.es.addInfiniteScrolling {
            [weak self] in
            self?.getData()
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isOffline{
            return coreData.getSavedItems().count
        }else{
            return itemsArray.count
        }
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier: String = "ItemTableViewCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! ItemTableViewCell
        if isOffline{
            cell.configCell(item: coreData.getSavedItems()[indexPath.row])
        }else{
            cell.configCell(item: itemsArray[indexPath.row])
        }
        cell.selectionStyle = .none
        return cell
    }
    
}
