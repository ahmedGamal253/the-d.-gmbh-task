//
//  ItemModel.swift
//  CashUTask
//
//  Created by Ahmed Gamal on 8/22/19.
//  Copyright © 2019 Ahmed Gamal. All rights reserved.
//

import Foundation

class ItemModel{
    
    var name = ""
    var description = ""
    var watchers_count = ""
    var language = ""
    
    init(_ response: [String:Any] = [String: Any]()) {
        let dic = HandleJSON.instance.handle(dicc: response)
        
        name = dic["name"] as? String ?? ""
        description = dic["description"] as? String ?? ""
        watchers_count = dic["watchers_count"] as? String ?? ""
        language = dic["language"] as? String ?? ""
    }
}
