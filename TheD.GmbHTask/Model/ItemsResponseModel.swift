//
//  ItemsResponseModel.swift
//  CashUTask
//
//  Created by Ahmed Gamal on 8/22/19.
//  Copyright © 2019 Ahmed Gamal. All rights reserved.
//

import Foundation

class ItemsResponseModel{
    
    var items = [ItemModel]()
    
    init(_ response: [String:Any] = [String: Any]()) {
        let dic = HandleJSON.instance.handle(dicc: response)
        for dic in dic["response"] as? [[String: Any]] ?? [[String: Any]]() {
            items.append(ItemModel(dic))
        }
    }
}
