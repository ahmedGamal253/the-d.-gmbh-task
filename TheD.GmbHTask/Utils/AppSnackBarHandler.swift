//
//  AppSnackBarHandler.swift
//  CashUTask
//
//  Created by Ahmed Gamal on 8/22/19.
//  Copyright © 2019 Ahmed Gamal. All rights reserved.
//

import Foundation
import TTGSnackbar

class AppSnackBarHandler {
    
    static let instance = AppSnackBarHandler()
    
    func showMessage(message:String)  {
        let snackbar = TTGSnackbar(message: "No internet connection", duration: .middle)
        snackbar.animationType = .slideFromTopBackToTop
        snackbar.show()
    }
    
}
