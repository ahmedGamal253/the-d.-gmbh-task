//
//  HandleJSON.swift
//  Yacoun
//
//  Created by Mohamed Elmaazy on 12/5/17.
//  Copyright © 2017 Internet Plus. All rights reserved.
//

import Foundation
import UIKit

class HandleJSON : NSObject {
    
    static let instance = HandleJSON()
    private override init() { }
    
    var navigationController:UINavigationController?
    
    func handle(dicc: [String: Any]) -> [String: Any] {
        var dic = dicc
        for (key, value) in dic {
            if value is NSNull {
                dic[key] = "" as Any
            } else if value is Int {
                let temp : Int =  value as! Int
                dic[key] = String(temp)
            } else if value is Double {
                let temp : Double = value as! Double
                dic[key] = String(temp)
            }
        }
        return dic
    }
    
    func checData(dic:[String:Any], key:String) -> Bool {
        if dic[key] != nil && dic[key] as? String != "" {
            return true
        }
        return false
    }

}
