//
//  AppConnectionsHandler.swift
//  Bahya
//
//  Created by mohamed elmaazy on 5/16/18.
//  Copyright © 2018 mohamed elmaazy. All rights reserved.
//

import Foundation
import Alamofire

public typealias ResponseClosure = (Int, [String:Any]?) -> Void

class AppConnectionsHandler {
    
    static let instance = AppConnectionsHandler()
    
    public var responseClosure:ResponseClosure?
    
    let sucess = 1
    let sucessWithError = 2
    let failServer = 3
    let failInternet = 4
    
    func get(url:String) {
        if checkConnection() {
            Alamofire.request(url, method: HTTPMethod.get, encoding: JSONEncoding.default).responseJSON { (response:DataResponse<Any>) in
                switch(response.result) {
                case .success(_):
                    if response.result.value != nil {
                        self.sucess(response: response)
                    } else {
                        self.fail(response: nil)
                    }
                    break
                case .failure(_):
                    self.fail(response: nil)
                    break
                }
            }
        } else {
            self.internetFail()
        }
    }
    
    func checkConnection() -> Bool {
        let reachabilityManager = Alamofire.NetworkReachabilityManager(host: "www.google.com")
        print((reachabilityManager?.isReachable)!)
        return (reachabilityManager?.isReachable)!
    }
    
    private func sucess(response:DataResponse<Any>) {
        if response.response?.statusCode == 200 {
            let dic = ["response": response.result.value!]
            responseClosure?(sucess, dic)
        } else {
            responseClosure?(failServer, nil)
        }
    }
    
    private func fail(response:[String : Any]?) {
        responseClosure?(failServer, nil)
    }
    
    private func internetFail() {
        responseClosure?(failInternet, nil)
    }
    
}
