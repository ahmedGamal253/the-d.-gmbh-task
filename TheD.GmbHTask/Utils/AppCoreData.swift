//
//  AppCoreData.swift
//  Safir El3rood
//
//  Created by mohamed elmaazy on 8/2/18.
//  Copyright © 2018 Internet Plus. All rights reserved.
//

import Foundation
import CoreData
import UIKit

class AppCoreData {
    
    static let instance = AppCoreData()
    
    func addItem(model:ItemModel) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let entity = NSEntityDescription.entity(forEntityName: "Item", in: context)
        let newOrder = NSManagedObject(entity: entity!, insertInto: context)
        newOrder.setValue(model.language, forKey: "language")
        newOrder.setValue(model.name, forKey: "name")
        newOrder.setValue(model.description, forKey: "descri")
        newOrder.setValue(model.watchers_count, forKey: "watchers_count")
        do {
            try context.save()
        } catch {
            print("Failed saving")
        }
    }
    
    func getSavedItems () -> [Item] {
        let fetchRequest: NSFetchRequest<Item> = Item.fetchRequest()
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        var items = [Item]()
        do {
            items =  try context.fetch(fetchRequest)
        }catch{
            print("error executing fetch request: \(error)")
        }
        return items
    }
    
    func removeItems() {
        let fetchRequest: NSFetchRequest<Item> = Item.fetchRequest()
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        do {
            let objects = try context.fetch(fetchRequest)
            for object in objects {
                context.delete(object)
            }
            try context.save()
        } catch _ {
            print("Failed saving after deleting")
        }
    }
}
